## FAQs

### Why is there an RSA Private Key in `docker-extras` directory?

`abuild` command does not offer a clean way of separating package building from
package signing. Therefore we need to have a RSA private key in the container.

Upstream Alpine Linux is working on fixing this issue.

https://lists.alpinelinux.org/alpine-devel/6057.html

See section - _separate out signing process of packages and index_

At this time, you should not rely on the integrity and authenticity
assertions of builder RSA Public Key.
